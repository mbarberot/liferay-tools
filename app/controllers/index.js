import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { XMLParser } from 'fast-xml-parser';
import generateOsgiProperties from 'liferay-tools/utils/properties-util';


export default class Index extends Controller {

  @tracked
  portletConfig = '';
  @tracked
  liferayPortletConfig = '';
  @tracked
  osgiProperties = {};
  @tracked
  osgiValues = [];


  @action
  submit(e) {
    e.preventDefault();

    const parser = new XMLParser();

    const portletData = this.portletConfig ? parser.parse(this.portletConfig)['portlet-app']['portlet'] : {};
    const liferayPortletData = this.liferayPortletConfig ? parser.parse(this.liferayPortletConfig)['liferay-portlet-app'] : {};

    this.osgiProperties = {
      portletName: portletData['portlet-name'],
      displayName: portletData['display-name'],
      viewTemplate: getInitParam(portletData, 'view-template'),
      configTemplate: getInitParam(portletData, 'config-template'),
      infoTitle: getObject(portletData, 'portlet-info').title,
      shortTitle: getObject(portletData, 'portlet-info')['short-title'],
      keywords: getObject(portletData, 'portlet-info').keywords,
      securityRoles: getArray(portletData, 'security-role-ref').map((role) => role['role-name']).join(','),
      instanceable: getObject(liferayPortletData, 'portlet')['instanceable'],
      addDefaultResource: getObject(liferayPortletData, 'portlet')['add-default-resource'],
      cssClassWrapper: getObject(liferayPortletData, 'css-class-wrapper'),
      raw: JSON.stringify({
        portlet: portletData,
        liferayPortlet: liferayPortletData,
      }, null, 2),
    }

    this.osgiValues = [
      `"com.liferay.portlet.display-category=CHANGE_ME"`,
      `"com.liferay.portlet.header-portlet-css=/css/main.css"`,
      `"com.liferay.portlet.instanceable=${this.osgiProperties.instanceable}"`,
      `"com.liferay.portlet.css-class-wrapper=${this.osgiProperties.cssClassWrapper}"`,
      `"javax.portlet.init-param.template-path=/"`,
      `"javax.portlet.init-param.view-template=${this.osgiProperties.viewTemplate}"`,
      `"javax.portlet.init-param.config-template=${this.osgiProperties.configTemplate}"`,
      '"javax.portlet.name=" + CHANGE_ME',
      '"javax.portlet.resource-bundle=content.Language"',
      `"javax.portlet.security-role-ref=${this.osgiProperties.securityRoles}"`,
      ...generateOsgiProperties({ portlet: portletData }),
    ];

    this.osgiValues.sort();
  }

}

function getInitParam(config, key) {
  const initParams = config['init-param'];

  if (initParams === undefined) {
    return {}
  }

  if (initParams instanceof Array) {
    return (initParams.filter(item => item.name === key)[0] || {}).value;
  }

  return getObject(config, 'init-param').value;
}

function getObject(config, key) {
  return (config[key] || {});
}

function getArray(config, key) {
  return (config[key] || []);
}

// TODO : should be configurable
const keyWhitelist = [
  "display-name",
  "portlet-info",
  "expiration-cache"
];

function isAllowedKey(key) {
  return keyWhitelist.includes(key);
}

function getAllowedKeys(obj) {
  return Object.keys(obj).filter(key => isAllowedKey(key));
}

const keyMapping = {
  "portlet-info": "info"
};

function transformKey(key) {
  return keyMapping[key] || key;
}

export default function transformToOsgiProperties(data) {

  const result = [];

  if (data.portlet) {
    const portlet = data.portlet;
    const allowedKeys = getAllowedKeys(portlet);

    for (const key of allowedKeys) {
      const value = portlet[key];
      const printableKey = transformKey(key);

      if(value instanceof Array) {
         continue;
      }

      if (value instanceof Object) {
        for(const subkey of Object.keys(value)) {
          const subValue = value[subkey];
          result.push(`"javax.portlet.${printableKey}.${subkey}=${subValue}"`)
        }
        continue;
      }

      result.push(`"javax.portlet.${printableKey}=${value}"`)
    }
  }

  return result;
}

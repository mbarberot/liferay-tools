import transformToOsgiProperties from 'liferay-tools/utils/properties-util';
import { module, test } from 'qunit';

module('Unit | Utility | properties-util', function () {

  test("transform portlet's simple keys", function (assert) {
    // given
    const data = {
      "portlet": {
        "display-name": "My Portlet Name",
        "expiration-cache": 0,
      }
    };

    // when 
    const result = transformToOsgiProperties(data);

    // then
    assert.deepEqual(result, [
      '"javax.portlet.display-name=My Portlet Name"',
      '"javax.portlet.expiration-cache=0"',
    ]);
  });

  test('do not transform all keys', function (assert) {
    // given
    const data = {
      "portlet": {
        "portlet-class": "com.liferay.util.bridges.mvc.MVCPortlet",
      }
    };

    // when 
    const result = transformToOsgiProperties(data);

    // then
    assert.deepEqual(result, []);
  });
  
  test("transform portlet's portlet-info", function (assert) {
    // given
    const data = {
      "portlet": {
        "portlet-info": {
          "title": "My Portlet",
          "short-title": "The Test Portlet",
          "keywords": "my test portlet name"
        },
      }
    };

    // when 
    const result = transformToOsgiProperties(data);

    // then
    assert.deepEqual(result, [
      '"javax.portlet.info.title=My Portlet"',
      '"javax.portlet.info.short-title=The Test Portlet"',
      '"javax.portlet.info.keywords=my test portlet name"',
    ]);
  });

  test("transform portlet's init-param", function (assert) {
    // given
    const data = {
      "portlet": {
        "portlet-class": "com.liferay.util.bridges.mvc.MVCPortlet",
        "init-param": [
          {
            "name": "view-template",
            "value": "/views/view.jsp"
          },
          {
            "name": "config-template",
            "value": "/views/configuration.jsp"
          }
        ],
      }
    };

    // when 
    const result = transformToOsgiProperties(data);

    // then
    assert.deepEqual(result, [ // TODO
      // "javax.portlet.init-param.view-template=/views/view.jsp",
      // "javax.portlet.init-param.config-template=/views/configuration.jsp",
    ]);
  });

});
